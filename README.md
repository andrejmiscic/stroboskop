# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://andrejmiscic@bitbucket.org/andrejmiscic/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/andrejmiscic/stroboskop/commits/56f254c290b82427a4a585c540318c192e35044f

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/andrejmiscic/stroboskop/commits/ad41376e410c93b717538a4a4e127df0d4538202

Naloga 6.3.2:
https://bitbucket.org/andrejmiscic/stroboskop/commits/11d945a486f4d2d44ca8ba6a0788252dcf0a4683

Naloga 6.3.3:
https://bitbucket.org/andrejmiscic/stroboskop/commits/7349c5333a58fd81109871923e1c345793624094

Naloga 6.3.4:
https://bitbucket.org/andrejmiscic/stroboskop/commits/2ec541291552be0d2a9b6790f92b63658a159faa

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/andrejmiscic/stroboskop/commits/977f13da1e9729964a93744783e6971f6c2abbf7

Naloga 6.4.2:
https://bitbucket.org/andrejmiscic/stroboskop/commits/181ef0338f9c854966f6fc30b24a3a7930d809bc

Naloga 6.4.3:
https://bitbucket.org/andrejmiscic/stroboskop/commits/0864d3a94a21e25dd6e7fe96f4c885a8b9c8f88e

Naloga 6.4.4:
https://bitbucket.org/andrejmiscic/stroboskop/commits/2962b2ffaea570242f8b62fe1765d2efd3347f21